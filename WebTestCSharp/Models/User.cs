using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;
using Azure.Core.GeoJson;

namespace WebTestCSharp.Models;

public class User
{
    [Key] [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int id { get; private set; }
    public string name { get; set; }
    public string username { get; set; }
    public string email { get; set; }
    
    public string phone { get; set; }
    public string website { get; set; }
    public Address Address { get; set; }
    
    public User(string name, string username, string email, Address address, string phone, string website)
    {
        this.name = name;
        this.username = username;
        this.email = email;
        this.Address = address;
        this.phone = phone;
        this.website = website;
    }
}