namespace WebTestCSharp.Models;

public class ReturnUser
{
    public int Id { get; set; }
    public string Address { get; set; }
    
    public ReturnUser(int id, string address)
    {
        this.Id = id;
        this.Address = address;
    }
}