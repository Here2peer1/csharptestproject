namespace WebTestCSharp.Models;

public class Address
{
    public string Street;
    public string Suite;
    public string City;
    public string Zipcode;
    public Geo Geo;

    public Address(string street, string suite, string city, string zipcode, Geo geo)
    {
        Street = street;
        Suite = suite;
        City = city;
        Zipcode = zipcode;
        Geo = geo;
    }
}