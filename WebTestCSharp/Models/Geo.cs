namespace WebTestCSharp.Models;

public class Geo
{
    public string lat { get; set; }
    public string lng { get; set; }
    
    public Geo(string lat, string lng)
    {
        this.lat = lat;
        this.lng = lng;
    }
}