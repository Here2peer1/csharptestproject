namespace WebTestCSharp.Models;

public class Company
{
    public string name { get; set; }
    public string catchPhrase { get; set; }
    public string bs { get; set; }
    
    public Company(string name, string catchPhrase, string bs)
    {
        this.name = name;
        this.catchPhrase = catchPhrase;
        this.bs = bs;
    }
}