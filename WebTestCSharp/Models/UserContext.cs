using Microsoft.EntityFrameworkCore;
using WebTestCSharp.Models;

namespace WebTestCSharp.Controllers;

public class UserContext : DbContext
{
    public UserContext(DbContextOptions<UserContext> options)
        : base(options)
    {
    }
    
    public DbSet<User> Users { get; set; }

}