using System.Diagnostics;
using System.Net;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebTestCSharp.Models;

namespace WebTestCSharp.Controllers;

[ApiController]
[Route("[controller]")]
public class UserController : ControllerBase
{
    private readonly ILogger<UserController> _logger;
    
    private readonly UserContext _context;

    public UserController(ILogger<UserController> logger, UserContext context)
    {
        _logger = logger;
        _context = context;
    }

    [HttpPost(Name = "Create user.")]
    public async Task<ActionResult<User>> PostUser(User user)
    {
        
        _context.Users.Add(user);
        await _context.SaveChangesAsync();
        
        return user;
    }
    
    
    // [HttpGet(Name = "Get users location.")]
    // public async Task<ActionResult<string>> GetUsersLocation(string guid)
    // {
    //     var selectedUser = await _context.Users.FirstOrDefaultAsync(user => user.Guid == Guid.Parse(guid));
    //     if (selectedUser == null)
    //     {
    //         return NotFound();
    //     }
    //     var usersLocation = selectedUser.City + " " + selectedUser.Street + " " + selectedUser.Postcode;
    //     return Ok(usersLocation);
    // }

    [HttpGet(Name = "Get external user.")]
    public async Task<ActionResult<ReturnUser>> ExternalUser(int id)
    {
        var json = await new HttpClient().GetAsync("https://jsonplaceholder.typicode.com/users");
        var content = await json.Content.ReadAsStringAsync();
        var jsonBody = JsonDocument.Parse(content);
        foreach (var person in jsonBody.RootElement.EnumerateArray())
        {
            if (person.GetProperty("id").GetInt32() != id) continue;
            var jsonString = person.GetProperty("address").GetProperty("city").GetString()+ " " + 
                             person.GetProperty("address").GetProperty("zipcode").GetString() + " " + 
                             person.GetProperty("address").GetProperty("geo").GetProperty("lat").GetString() + " " + 
                             person.GetProperty("address").GetProperty("geo").GetProperty("lng").GetString();

            var returnUserObject = new ReturnUser(id, jsonString);
            return Ok(returnUserObject);
        }

        return NotFound("User with id "+ id + " does not exist.");
    }
}